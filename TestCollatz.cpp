// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}


TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(591, 327));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  591);
    ASSERT_EQ(j, 327);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(500, 600));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 500);
    ASSERT_EQ(j, 600);
    ASSERT_EQ(v, 137);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(658, 844));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  658);
    ASSERT_EQ(j, 844);
    ASSERT_EQ(v, 171);
}


// -----
// single
// -----
// Tests for my helper function that calculates the collatz cycle length for a given number
TEST(CollatzFixture, single0) {
    ASSERT_EQ(collatz_single(7815), 133);
}

TEST(CollatzFixture, single1) {
    ASSERT_EQ(collatz_single(99999), 227);
}

TEST(CollatzFixture, single2) {
    ASSERT_EQ(collatz_single(2856), 36);
}

TEST(CollatzFixture, single3) {
    ASSERT_EQ(collatz_single(4096), 13);
}

TEST(CollatzFixture, single4) {
    ASSERT_EQ(collatz_single(1), 1);
}

// -----
// range
// -----
//Tests for my helper function that calculates the max collatz cycle length between two ints inclusive, without
//preprocessing - assumes that the first parameter is less than the second
TEST(CollatzFixture, range0) {
    ASSERT_EQ(collatz_range(400, 500), 142);
}

TEST(CollatzFixture, range1) {
    ASSERT_EQ(collatz_range(78, 134), 122);
}

TEST(CollatzFixture, range2) {
    ASSERT_EQ(collatz_range(1, 999), 179);
}

TEST(CollatzFixture, range3) {
    ASSERT_EQ(collatz_range(44, 77), 116);
}

TEST(CollatzFixture, range4) {
    ASSERT_EQ(collatz_range(800, 854), 135);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
