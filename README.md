# CS371g: Generic Programming Collatz Repo

* Name: Kaushik Malapati

* EID: KM48453

* GitLab ID: KaushikMalapati

* HackerRank ID: KaushikMalapati

* Git SHA: 24f90c6e73c41b56da737140867c5cf15cd7f1d1

* GitLab Pipelines: https://gitlab.com/KaushikMalapati/cs371g-collatz/-/pipelines

* Estimated completion time: 10

* Actual completion time: 17

* Comments: Submitted one day late. 
