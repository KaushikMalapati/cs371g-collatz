// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param an istream_iterator
 * @return a pair of ints
 */
pair<int, int> collatz_read (istream_iterator<int>&);

// ------------
// collatz_single
// ------------

/**
 * @param a long to perform the Collatz conjecture on
 * @return the integer Collatz cycle length of that integer
 */
int collatz_single(long n);

// ------------
// collatz_range
// ------------

/**
 * @param lowerBound an integer
 * @param upperBound an integer
 * @return the max Collatz cycle lentgth between lowerBound and upperBound inclusive
 */
int collatz_range(int lowerBound, int upperBound);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
